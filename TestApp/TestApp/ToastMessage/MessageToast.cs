﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Interfaces;
using Xamarin.Forms;

namespace TestApp.ToastMessage
{
    /// <summary>
    /// Clase para consumir desde los pages y mostrar los mensajes
    /// </summary>
    public static class MessageToast
    {
        public static void Message(string message)
        {
            if (Device.OS == TargetPlatform.Android)
            {
                DependencyService.Get<IShowToast>().ShowToast(message);
            }
        }
    }
}
