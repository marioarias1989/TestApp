﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.ViewModel;

using Xamarin.Forms;

namespace TestApp.Pages
{
    public partial class PhotoVisorPage : ContentPage
    {
        #region Builder
        public PhotoVisorPage(PhotoViewModel photo)
        {
            InitializeComponent();
            this.Photo = photo;
            DisplayPhoto();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Obtiene la foto
        /// </summary>
        private PhotoViewModel Photo { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Metodo que se encarga de visualizar la foto
        /// </summary>
        private void DisplayPhoto()
        {
            if(this.Photo != null)
            {
                this.imageDisplay.Source = this.Photo.ImageSource;
            }
        }
        #endregion

        #region Handlers
        /// <summary>
        /// Evento que se dispara al presionar exit de la barra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolbarExit_Clicked(object sender, EventArgs e)
        {
            App.Navigator.PopAsync();
        }
        #endregion
    }
}
