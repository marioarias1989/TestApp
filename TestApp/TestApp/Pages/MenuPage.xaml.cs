﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.ViewModel;
using TestApp.Services;
using TestApp.ToastMessage;

using Xamarin.Forms;

namespace TestApp.Pages
{
    public partial class MenuPage : ContentPage
    {
        #region Builder
        public MenuPage()
        {
            InitializeComponent();
            this.buttonEndSession.Clicked += ButtonEndSession_Clicked;
            LoadMenuData();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Obtiene el usuario
        /// </summary>
        private UserLoginViewModel User { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Metodo que carga la información del menu
        /// </summary>
        private void LoadMenuData()
        {
            this.User = new UserService().GetUser();
            if(this.User == null)
            {
                MessageToast.Message("El usuario no se encuentra en la BD local");
                return;
            }
            this.labelName.Text = "Nombre: " + this.User.nameUser;
            this.labelLastName.Text = "Apellido: " + this.User.lastNameUser;
            this.labelEmail.Text = "Correo: " + this.User.emailUser;
            this.labelNumber.Text = "Documento: " + this.User.documentUser;
        }

        /// <summary>
        /// Cierra la sesión del usuario
        /// </summary>
        private void EndSession()
        {
            var service = new UserService();
            service.DeleteUser(this.User);
            App.Current.MainPage = new LoginPage();
        }
        #endregion

        #region Handlers
        /// <summary>
        /// Evento que se ejecuta al presiona click sobre el botón de cerrar sesión
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonEndSession_Clicked(object sender, EventArgs e)
        {
            EndSession();
        }
        #endregion
    }
}
