﻿using Android.Webkit;
using Geolocator.Plugin;
using Java.Lang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.ToastMessage;
using TestApp.ViewModel;

using Xamarin.Forms;

namespace TestApp.Pages
{
    public partial class MainPage : ContentPage
    {
        #region Builder
        public MainPage()
        {
            InitializeComponent();
            this.toolbarPhoto.Clicked += ToolbarPhoto_Clicked;
            this.listViewPhotos.ItemTapped += ListViewPhotos_ItemSelected;
        }
        #endregion

        #region Variables
        /// <summary>
        /// Listado de photos
        /// </summary>
        private List<PhotoViewModel> ListPhotos;
        #endregion

        #region Handlers
        /// <summary>
        /// Evento que se dispara al presionar click sobre el boton photo de la barra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ToolbarPhoto_Clicked(object sender, EventArgs e)
        {
            await TakePhoto();
        }

        /// <summary>
        /// Evento que se dispara al seleccionar un item del listView para poder visualizar la foto en un page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListViewPhotos_ItemSelected(object sender, ItemTappedEventArgs e)
        {
            //Se obtiene el item seleccionado y se envia al page visor
            OpenPhoto((PhotoViewModel)e.Item);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Metodo que abre la camara para tomar la foto
        /// </summary>
        /// <returns></returns>
        private async Task TakePhoto()
        {
            //Se indica el spin de await
            ShowAwait(true);

            //Abre la camara para tomar la foto
            var photo = await Media.Plugin.CrossMedia.Current.TakePhotoAsync(new Media.Plugin.Abstractions.StoreCameraMediaOptions() { });

            //Se valida si no hay foto
            if (photo == null)
            {
                //Se indica el spin de await
                ShowAwait(false);
                return;
            }
            
            //Se valida si el listado de fotos es nulo
            if(ListPhotos == null)
            {
                ListPhotos = new List<PhotoViewModel>();
            }
            
            //Se agrega un item al listado
            ListPhotos.Add(new PhotoViewModel {
                ImageSource = ImageSource.FromStream(() => photo.GetStream()),
                IsProfilePhoto = false,
                Position = ListPhotos.Count + 1});

            //Se indica el spin de await
            ShowAwait(false);

            //Se carga el source del listView
            this.listViewPhotos.ItemsSource = null;
            this.listViewPhotos.ItemsSource = ListPhotos;
        }

        /// <summary>
        /// Metodo que abre un nuevo page con la foto
        /// </summary>
        private void OpenPhoto(PhotoViewModel photo)
        {
            var photoVisor = new PhotoVisorPage(photo);
            App.Navigator.PushAsync(photoVisor);
        }

        /// <summary>
        /// Muestra el await indicator
        /// </summary>
        private void ShowAwait(bool isVisible)
        {
            this.indicatorAwait.IsVisible = isVisible;
            this.indicatorAwait.IsRunning = isVisible;
        }
        #endregion
    }
}
