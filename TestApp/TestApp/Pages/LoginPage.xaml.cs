﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.ToastMessage;
using TestApp.Result;
using TestApp.Services;
using TestApp.ViewModel;

using Xamarin.Forms;

namespace TestApp.Pages
{
    public partial class LoginPage : ContentPage
    {
        #region Builder
        public LoginPage()
        {
            InitializeComponent();
            this.buttonLogin.Clicked += ButtonLogin_Clicked;
            this.Navigator = this.Navigation;
        }
        #endregion

        #region Variables
        /// <summary>
        /// Se navega hacia otros pages
        /// </summary>
        private INavigation Navigator { get; set; }
        #endregion

        #region Handlers
        /// <summary>
        /// Evento que se dispara al presionar click sobre el botón de Iniciar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonLogin_Clicked(object sender, EventArgs e)
        {
            ValidateUserLogin();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Consume el metodo que valida al usuario
        /// </summary>
        private async void ValidateUserLogin()
        {
            //Se inicia el activityIndicator
            ShowHideControls(false);

            //Se consume el servicio para validar el usuario
            ActionResult resultLogin = await new LoginUserService(this.textUser.Text, this.textPass.Text).ValidateLoginUser();

            //Se valida que no hayan habido errores
            if (resultLogin.StatusResult == Status.Warning || resultLogin.StatusResult == Status.Error)
            {
                MessageToast.Message(resultLogin.MessageResult);
                ShowHideControls(true);
                return;
            }

            //Si no hubo errores en el logueo se pasa a crear el usuario en la BD local
            ActionResult resultCreateUser = await new LoginUserService((UserLoginViewModel)resultLogin.ObjectEmbebed).CreateUserInBD();

            //Se valida que no hayan habido errores
            if (resultCreateUser.StatusResult == Status.Warning || resultCreateUser.StatusResult == Status.Error)
            {
                MessageToast.Message(resultCreateUser.MessageResult);
                ShowHideControls(true);
                return;
            }

            //Si no hubo errores se redirecciona al page principal de la app
            App.Current.MainPage = new MasterPage();
        }

        /// <summary>
        /// Muestra y oculta el boton de iniciar
        /// </summary>
        /// <param name="isShow"></param>
        private void ShowHideControls(bool isShow)
        {
            this.buttonLogin.IsVisible = isShow;
            this.indicatorAwait.IsRunning = !isShow;
        }
        #endregion
    }
}
