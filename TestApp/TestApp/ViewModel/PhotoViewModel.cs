﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TestApp.ViewModel
{
    public class PhotoViewModel
    {
        /// <summary>
        /// Establece si fue todo ok
        /// </summary>
        public ImageSource ImageSource { get; set; }

        /// <summary>
        /// Establece si es la foto de perfil
        /// </summary>
        public bool IsProfilePhoto { get; set; }

        /// <summary>
        /// Permite saber la posición de la foto
        /// </summary>
        public int Position { get; set; }
    }
}
