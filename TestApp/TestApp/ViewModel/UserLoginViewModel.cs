﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.ViewModel
{
    public class UserLoginViewModel
    {
        /// <summary>
        /// Establece el id del usuario
        /// </summary>
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        /// <summary>
        /// Establece si fue todo ok
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// Establece si hubo error
        /// </summary>
        public bool error { get; set; }

        /// <summary>
        /// Establece el mensaje
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// Retorna el token
        /// </summary>
        public string token { get; set; }

        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string nameUser { get; set; }

        /// <summary>
        /// Apellido del usuario
        /// </summary>
        public string lastNameUser { get; set; }

        /// <summary>
        /// Correo del usuario
        /// </summary>
        public string emailUser { get; set; }

        /// <summary>
        /// Documento del usuario
        /// </summary>
        public string documentUser { get; set; }
    }
}
