﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Result
{
    /// <summary>
    /// Clase que se encarga de devolver resultado cuando el usuario ejecuta una acción
    /// </summary>
    public class ActionResult
    {
        /// <summary>
        /// Estado que se retorna
        /// </summary>
        public Status StatusResult { get; set; }

        /// <summary>
        /// Mensaje que se retorna
        /// </summary>
        public string MessageResult { get; set; }

        /// <summary>
        /// Objeto que se retorna
        /// </summary>
        public object ObjectEmbebed { get; set; }
    }

    /// <summary>
    /// Enumeración para el estado del ActionResult
    /// </summary>
    public enum Status
    {
        Success = 1,
        Warning = 2,
        Error = 3
    }
}
