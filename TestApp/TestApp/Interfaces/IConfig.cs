﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Interop;

namespace TestApp.Interfaces
{
    /// <summary>
    /// Interface para la configuración de la BD local
    /// </summary>
    public interface IConfig
    {
        /// <summary>
        /// Se obtiene el directorio
        /// </summary>
        string DirectoryDB { get; }

        /// <summary>
        /// Se obtiene la plataforma
        /// </summary>
        ISQLitePlatform Platform { get; }
    }
}
