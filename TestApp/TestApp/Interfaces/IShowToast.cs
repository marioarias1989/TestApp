﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Interfaces
{
    /// <summary>
    /// Interfaz que sirve para implementar en cada plataforma
    /// </summary>
    public interface IShowToast
    {
        /// <summary>
        /// Este método es para mostrar los mensajes en forma de toast
        /// </summary>
        /// <param name="message"></param>
        void ShowToast(string message);
    }
}
