﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using Xamarin.Forms;
using System.IO;
using TestApp.ViewModel;
using TestApp.Interfaces;

namespace TestApp.Config
{
    /// <summary>
    /// Clase para poder acceder a la BD local
    /// </summary>
    public class DataAcces : IDisposable
    {
        #region Variables
        /// <summary>
        /// Conexión a la BD
        /// </summary>
        private SQLiteConnection Connection;

        #endregion

        #region Builder
        /// <summary>
        /// Constructor
        /// </summary>
        public DataAcces()
        {
            var config = DependencyService.Get<IConfig>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoryDB, "TestApp.db3"));
            Connection.CreateTable<UserLoginViewModel>();
            //Connection.CreateTable<BudgetDetailViewModel>();
            //Connection.CreateTable<BudgetDetailExpensesViewModel>();
        }

        #endregion

        #region Properties
        /// <summary>
        /// Conexion de la BD
        /// </summary>
        public SQLiteConnection Connect
        {
            get
            {
                return this.Connection;
            }
        }
        #endregion

        #region GenericMethods
        /// <summary>
        /// Metodo generico para insertar
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public void Insert<T>(T entity)
        {
            Connection.Insert(entity);
        }

        /// <summary>
        /// Metodo generico para actualizar
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public void Update<T>(T entity)
        {
            Connection.Update(entity);
        }

        /// <summary>
        /// Metodo generico para eliminar
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public void Delete<T>(T entity)
        {
            Connection.Delete(entity);
        }
        #endregion

        #region OthersMethods
        //public BudgetViewModel GetBudgetByMonthAndYear(int Month, int Year)
        //{
        //    return Connection.Table<BudgetViewModel>().FirstOrDefault(x => x.Month == Month && x.Year == Year);
        //}

        //public List<BudgetIncomeViewModel> ListAllIncome()
        //{
        //    return Connection.Table<BudgetIncomeViewModel>().OrderByDescending(x => x.Id).ToList();
        //}

        //public List<BudgetIncomeViewModel> ListIncomeByMonthAndYear(int year, int month)
        //{
        //    return Connection.Table<BudgetIncomeViewModel>().Where(x => x.Year == year && x.Month == month).OrderByDescending(x => x.Id).ToList();
        //}

        //public decimal SumIncomeByMonthAndYear(int Month, int Year)
        //{
        //    return (from x in Connection.Table<BudgetIncomeViewModel>() where x.Month == Month && x.Year == Year select x.Value).Sum();
        //}
        #endregion

        #region Dispose
        /// <summary>
        /// Se rompe la conexión
        /// </summary>
        public void Dispose()
        {
            Connection.Dispose();
        }

        #endregion
    }
}
