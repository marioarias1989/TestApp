﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestApp.Pages;
using Xamarin.Forms;
using TestApp.ViewModel;
using TestApp.Services;

namespace TestApp
{
    public partial class App : Application
    {
        public static NavigationPage Navigator { get; internal set; }

        public App()
        {
            InitializeComponent();

            var user = new UserService().GetUser();
            if(user != null)
            {
                MainPage = new MasterPage();
            }
            else
            {
                MainPage = new LoginPage();
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
