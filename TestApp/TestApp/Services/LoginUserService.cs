﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TestApp.Result;
using TestApp.ViewModel;
using TestApp.Config;

namespace TestApp.Services
{
    /// <summary>
    /// Servicio para autenticar el usuario
    /// </summary>
    public class LoginUserService
    {
        #region Builder
        /// <summary>
        /// Constructor para el inicio de sesion
        /// </summary>
        public LoginUserService(string emailLogin, string passLogin)
        {
            this.EmailLogin = emailLogin;
            this.PassLogin = passLogin;
        }

        /// <summary>
        /// Constructor para guardar el usuario en la BD
        /// </summary>
        /// <param name="userEntity"></param>
        public LoginUserService(UserLoginViewModel userEntity)
        {
            this.UserLogin = userEntity;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Obtiene el usuario
        /// </summary>
        private string EmailLogin { get; set; }

        /// <summary>
        /// Obtiene la constraseña
        /// </summary>
        private string PassLogin { get; set; }

        /// <summary>
        /// Obtiene el usuario
        /// </summary>
        private UserLoginViewModel UserLogin { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Método para validar el usuario y poderse autenticar
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> ValidateLoginUser()
        {
            //Se valida que hayan escrito usuario
            if(string.IsNullOrEmpty(this.EmailLogin))
            {
                return new ActionResult { StatusResult = Status.Warning, MessageResult = "Debe ingresar un usuario", ObjectEmbebed = null };
            }

            //Se valida que hayan escrito contraseña
            if (string.IsNullOrEmpty(this.PassLogin))
            {
                return new ActionResult { StatusResult = Status.Warning, MessageResult = "Debe ingresar una contraseña", ObjectEmbebed = null };
            }

            try
            {
                using (HttpClient clientHttp = new HttpClient())
                {
                    //Url que se consume
                    string url = "https://serveless.proximateapps-services.com.mx/catalog/dev/webadmin/authentication/login";

                    //Se convierte los datos a un array
                    string content = "{correo:" + this.EmailLogin + ",contrasenia:" + this.PassLogin + "}";

                    //Se serializa el array
                    //string content = JsonConvert.SerializeObject(userContent);

                    //Se establece el body
                    StringContent body = new StringContent(content, Encoding.UTF8, "application/json");

                    //Se envia la url y el body
                    var result = await clientHttp.PostAsync(url, body);

                    //Si valida si hubo problemas de conexión
                    if (!result.IsSuccessStatusCode)
                    {
                        return new ActionResult { StatusResult = Status.Error, MessageResult = "Problemas de conexión con el servidor", ObjectEmbebed = null };
                    }

                    //Se obtiene el resultado del webService
                    string data = await result.Content.ReadAsStringAsync();

                    //Se convierte el resultado del webService a una entidad
                    var userResult = JsonConvert.DeserializeObject<UserLoginViewModel>(data);

                    //Se valida si hubo errores de autenticación
                    if (userResult.success == false)
                    {
                        return new ActionResult { StatusResult = Status.Warning, MessageResult = userResult.message, ObjectEmbebed = userResult };
                    }

                    //Si no hubo errores de autenticación retornamos ok
                    return new ActionResult { StatusResult = Status.Success, MessageResult = userResult.message, ObjectEmbebed = userResult };
                }
            }
            catch(Exception ex)
            {
                //Retorna error si hay un catch
                return new ActionResult { StatusResult = Status.Error, MessageResult = "Problemas de conexión con el servidor", ObjectEmbebed = null };
            }
        }

        /// <summary>
        /// Crea el usuario en la BD local
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> CreateUserInBD()
        {
            //Se declara el objeto para obtener el json
            WebServiceObject resultJson = null;

            try
            {
                using (HttpClient clientHttp = new HttpClient())
                {
                    //Url para consumir el webService
                    string url = "https://serveless.proximateapps-services.com.mx/catalog/dev/webadmin/users/getdatausersession";

                    //Configuración del client y header
                    clientHttp.DefaultRequestHeaders.Clear();
                    clientHttp.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    clientHttp.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("utf-8"));
                    clientHttp.DefaultRequestHeaders.Add("Authorization", this.UserLogin.token);
                    
                    //Se establece el token
                    string UserData = "{Authorization:" + this.UserLogin.token + "}";

                    //Se establece el body
                    StringContent body = new StringContent(UserData, Encoding.UTF8, "application/json");

                    //Se consume la url
                    var result = await clientHttp.PostAsync(url, body);

                    //Se obtiene la data de la url
                    string data = await result.Content.ReadAsStringAsync();

                    //Se valida si hubo algun error
                    if (result.IsSuccessStatusCode)
                    {
                        //Se obtiene el resultado del json
                        resultJson = JsonConvert.DeserializeObject<WebServiceObject>(data);

                        //Se valida si el json retorno error
                        if (resultJson.success == false)
                        {
                            return new ActionResult { StatusResult = Status.Warning, MessageResult = resultJson.message, ObjectEmbebed = null };
                        }

                    }
                    else
                    {
                        return new ActionResult { StatusResult = Status.Error, MessageResult = "Problemas con la conexión al servidor", ObjectEmbebed = null };
                    }

                }

                //Se asignan las propiedades que hacen falta al objeto de usuario
                this.UserLogin.nameUser = resultJson.data[0].nombres;
                this.UserLogin.lastNameUser = resultJson.data[0].apellidos;
                this.UserLogin.emailUser = resultJson.data[0].correo;
                this.UserLogin.documentUser = resultJson.data[0].numero_documento;

                //Si no hubo error se procede a guardar al usuario en la BD local
                var service = new UserService();
                service.SaveUser(this.UserLogin);
                
                return new ActionResult { StatusResult = Status.Success, MessageResult = "Usuario registrado correctamente", ObjectEmbebed = this.UserLogin };
            }
            catch (Exception ex)
            {
                return new ActionResult { StatusResult = Status.Error, MessageResult = "Problemas con la conexión al servidor", ObjectEmbebed = null };
            }
        }
        #endregion
    }

    /// <summary>
    /// Clase para obtener el contenido del webservice
    /// </summary>
    public class WebServiceObject
    {
        /// <summary>
        /// Establece si fue todo ok
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// Establece si hubo error
        /// </summary>
        public bool error { get; set; }

        /// <summary>
        /// Establece el mensaje
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// Listado de la persona
        /// </summary>
        public List<PersonObject> data { get; set; }
    }

    /// <summary>
    /// Clase para obtener el objeto de persona
    /// </summary>
    public class PersonObject
    {
        /// <summary>
        /// Establece el nombre de la persona
        /// </summary>
        public string nombres { get; set; }

        /// <summary>
        /// Establece el apellido de la persona
        /// </summary>
        public string apellidos { get; set; }

        /// <summary>
        /// Establece el correo de la persona
        /// </summary>
        public string correo { get; set; }

        /// <summary>
        /// Establece el numero documento de la persona
        /// </summary>
        public string numero_documento { get; set; }
    }
}
