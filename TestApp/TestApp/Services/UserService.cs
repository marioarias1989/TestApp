﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Config;
using TestApp.ViewModel;

namespace TestApp.Services
{
    /// <summary>
    /// Clase que se encarga del CRUD del user
    /// </summary>
    public class UserService
    {
        #region Builder
        public UserService()
        {

        }
        #endregion

        #region Methods
        /// <summary>
        /// Metodo que guarda al user
        /// </summary>
        /// <returns></returns>
        public void SaveUser(UserLoginViewModel user)
        {
            using (var data = new DataAcces())
            {
                data.Insert<UserLoginViewModel>(user);
            }
        }

        /// <summary>
        /// Obtiene el usuario que esta logueado
        /// </summary>
        /// <returns></returns>
        public UserLoginViewModel GetUser()
        {
            using (var data = new DataAcces())
            {
                return data.Connect.Table<UserLoginViewModel>().FirstOrDefault();
            }
        }

        /// <summary>
        /// Elimina el usuario
        /// </summary>
        /// <param name="user"></param>
        public void DeleteUser(UserLoginViewModel user)
        {
            using (var data = new DataAcces())
            {
                data.Delete<UserLoginViewModel>(user);
            }
        }
        #endregion
    }
}
