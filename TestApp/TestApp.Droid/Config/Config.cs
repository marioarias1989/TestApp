using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite.Net.Interop;
using Xamarin.Forms;
using TestApp.Interfaces;

[assembly: Dependency(typeof(TestApp.Droid.Config.Config))]

namespace TestApp.Droid.Config
{
    /// <summary>
    /// Clase que utiliza el device para la BD
    /// </summary>
    public class Config : IConfig
    {
        /// <summary>
        /// Obtiene el directorio
        /// </summary>
        private string directoryDB;

        /// <summary>
        /// Obtiene la plataforma
        /// </summary>
        private ISQLitePlatform platform;

        /// <summary>
        /// Se asigna el directorio
        /// </summary>
        public string DirectoryDB
        {
            get
            {
                if (string.IsNullOrEmpty(directoryDB))
                {
                    directoryDB = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }
                return directoryDB;
            }
        }

        /// <summary>
        /// Se asigna la plataforma
        /// </summary>
        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
                }
                return platform;
            }
        }
    }
}