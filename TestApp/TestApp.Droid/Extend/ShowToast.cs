using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TestApp.Interfaces;
using Xamarin.Forms;
using Plugin.CurrentActivity;

//Inyectamos la dependencia
[assembly: Dependency(typeof(TestApp.Droid.Extend.ShowToast))]

namespace TestApp.Droid.Extend
{
    /// <summary>
    /// Clase que sirve para mostrar el mensaje toast
    /// </summary>
    public class ShowToast : IShowToast
    {
        /// <summary>
        /// M�todo para mostrar el mensaje toast
        /// </summary>
        /// <param name="message"></param>
        void IShowToast.ShowToast(string message)
        {
            Activity activity = CrossCurrentActivity.Current.Activity;
            Toast.MakeText(Forms.Context, message, ToastLength.Long).Show();
        }
    }
}